# coding=utf-8
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
# import SocketServer
import json
from selenium_utils import Selenium


class S(BaseHTTPRequestHandler):
    def _set_headers(self, content_type):
        if not content_type:
            content_type = 'text/html'
        self.send_response(200)
        self.send_header('Content-type', content_type)
        self.end_headers()

    def do_GET(self):
        coeso_web = Selenium("http://mapa.coesa.com/webinfo/")
        coeso_web.wait(5)
        # coeso_web.select("itCbox", "110 - São Gonçalo x Passeio")
        # coeso_web.wait(3)
        # coeso_web.select("varCbox", "São Gonçalo x Passeio")
        # coeso_web.wait(3)
        # coeso_web.click("mostrarItinerario")
        # coeso_web.wait(3)

        rest_API_url = coeso_web.driver.execute_script("return restAPIUrl;")
        api_token = json.dumps({"token": rest_API_url.split('/')[5]})

        coeso_web.stop()
        if api_token:
            self._set_headers('application/json')
            self.wfile.write(api_token)
        else:
            self.send_error(404, 'Mothership panic!!')

    def do_HEAD(self):
        self._set_headers()

    def do_POST(self):
        # content_length = int(self.headers['Content-Length'])  # <--- Gets the size of data
        # post_data = self.rfile.read(content_length)  # <--- Gets the data itself

        self._set_headers()
        self.wfile.write("<html><body><h1>POST!</h1></body></html>")


def run(server_class=HTTPServer, handler_class=S, port=8080):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print 'Starting httpd...'
    httpd.serve_forever()

if __name__ == "__main__":
    from sys import argv

    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()
