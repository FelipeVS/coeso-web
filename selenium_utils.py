# coding=utf-8
from selenium import webdriver
from selenium.webdriver.support.ui import Select


class Selenium():
    driver = None

    def __init__(self, url):
        print 'Instanciando o Selenium'
        self.driver = webdriver.PhantomJS()
        self.driver.set_window_size(800, 600)
        self.driver.get(url)

    def stop(self):
        self.driver.close()
        self.driver.quit()

    def wait(self, number):
        self.driver.implicitly_wait(number)

    def click(self, element):
        self.driver.find_element_by_id(element).click()

    def select(self, element, option):
        select = Select(self.driver.find_element_by_id(element))
        select.select_by_visible_text(option)
